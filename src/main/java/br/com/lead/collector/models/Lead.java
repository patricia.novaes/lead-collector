package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties(value = {"data"}, allowGetters = true)
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotBlank(message = "O nome não pode ser vazio")
    @NotNull(message = "O campo nome não pode ser nulo")
    private String nome;

    @Email(message = "Formato do email inválido")
    @NotNull(message = "O campo email não pode ser nulo")
    private String email;

    private LocalDate data;

    private TipoLeadEnum tipoLead;

    @ManyToMany (cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead(){

    }

    public int getId() {
        return id;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
