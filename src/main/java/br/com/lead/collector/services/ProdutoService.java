package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto buscarPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if(optionalProduto.isPresent()){
            return optionalProduto.get();
        }
        throw new RuntimeException("Produto não encontrado!!");
    }

    public Produto atualizarProduto(int id, Produto produto){
        if(produtoRepository.existsById(id)){
            produto.setId(id);
            Produto produtoObjeto = salvarProduto(produto);
            return produtoObjeto;
        }
        throw new RuntimeException("Produto não encontrado!!");
    }


    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        }else{
            throw new RuntimeException("Produto não encontrado!!");
        }
    }


    public Iterable<Produto> buscarTodosPorNome(String nome){
        Iterable<Produto> produtos = produtoRepository.findAllByNomeContains(nome);
        return produtos;
    }

    public List<Produto> buscarTodosPorId(List<Integer> id){
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List) produto;
    }
}










