package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNome("Patricia");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("pati@gmail.com");

        produto = new Produto();
        produto.setId(1);
        produto.setDescricao("Café do bom!");
        produto.setNome("Café");
        produto.setPreco(30.0);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarSalvarLead(){
        Mockito.when(produtoService.buscarTodosPorId(Mockito.anyList())).thenReturn(produtos);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadTest = new Lead();
        leadTest.setNome("Pati");
        leadTest.setProdutos(produtos);
        leadTest.setEmail("pati@pati.com");
        leadTest.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadTest);

        Lead leadObjeto = leadService.salvarLead(leadTest);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));

    }

    @Test
    public void testarBuscarPorTodosOsLeads(){
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarBuscarTodosPorTipoLead(){
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAllByTipoLead(lead.getTipoLead())).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodosPorTipoLead(lead.getTipoLead());

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadTest = new Lead();
        leadTest.setNome("Pati");
        leadTest.setProdutos(produtos);
        leadTest.setEmail("pati@pati.com");
        leadTest.setTipoLead(TipoLeadEnum.FRIO);

        Lead leadTeste = leadService.buscarPorId(leadTest.getId());
        Assertions.assertEquals(leadOptional.get().getId(), leadTeste.getId());
    }

    @Test
    public void testarAtualizarLead(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Chocolate");
        leadTeste.setTipoLead(TipoLeadEnum.ORGANICO);
        leadTeste.setEmail("chocolate@chocolate.com.br");
        leadTeste.setProdutos(produtos);

        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);
        Lead leadAtualizado = leadService.atualizarLead(leadTeste.getId(), leadTeste);

        Assertions.assertNotNull(leadAtualizado);
    }


    @Test
    public void testarDeletarLead() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);

        leadService.deletarLead(8001);

        // Verifica quatas vezes esse methodo do repository foi chamado quando eu executei o meu metodo que quero testar
        // o leadService.deletarLead()
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }


    @Test
    public void testarDeletarLeadNegativo() {
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.deletarLead(8001);});
    }




}